﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InstructionManager : MonoBehaviour {

	public Canvas iCanvas;
	public Canvas PopupCanvas;
	public Button iButton;
	int slide = 0;
	// Use this for initialization
	void Start () {
		
	}

	public void Next () {
		if (slide == 4) {
			slide = 0;
		}
		else{
			slide++;
		}
		Image[] img = iCanvas.GetComponentsInChildren<Image>();
		Debug.Log (img.Length);
		foreach (Image data in img) {
			data.enabled = false;
		}
		img [slide].enabled = true;
	}

	public void Before () {
		if (slide == 0) {
			slide = 4;
		}
		else{
			slide--;
		}
		Image[] img = iCanvas.GetComponentsInChildren<Image>();

		foreach (Image data in img) {
			data.enabled = false;
		}
		img [slide].enabled = true;
	}

	public void popClick(){
		Image[] img = PopupCanvas.GetComponentsInChildren<Image>();
		img [slide].enabled = true;
		iButton.gameObject.SetActive (true);
	}

	public void closePop(){
		Image[] img = PopupCanvas.GetComponentsInChildren<Image>();
		img [slide].enabled = false;
		iButton.gameObject.SetActive (false);
	}

	public void backtoMap(){
		Application.LoadLevel (17);
	}


}

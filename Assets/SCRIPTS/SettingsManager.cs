﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour 
{
	public Slider iSlider;

	void Awake(){
		if (iSlider) {
			AudioListener.volume = PlayerPrefs.GetFloat ("Volume");
			iSlider.value = AudioListener.volume;
		}
	}

	public void VolumeControl()
	{
		AudioListener.volume = iSlider.value;
		PlayerPrefs.SetFloat ("Volume", AudioListener.volume);
		PlayerPrefs.Save ();
	}

	public void backtoMenu(){
		Application.LoadLevel (56);
	}

	public void deleteUser()
	{
		string user = PlayerPrefs.GetString ("CurrentUser");
		string users = PlayerPrefs.GetString ("Users");

		string[] userlist = users.Split (',');
		string newlist = "";
		int loop = 1;
		Debug.Log ("Users count: " + userlist.Length);
		foreach (string data in userlist) {
			Debug.Log ("currentdata: " + data + " and userdata:" + user);
			if (data != user) {
				if (loop == 1) {
					newlist += data; 
					Debug.Log ("newlistadd: " + newlist);
					loop++;
				} else {
					newlist += "," + data; 
					Debug.Log ("newlistadd: " + newlist);	
				}
			}
		}
		Debug.Log ("newlist: " + newlist);
		PlayerPrefs.SetString ("Users", newlist);
		PlayerPrefs.Save ();
		int counter = 1;
		while(counter > 12){
			PlayerPrefs.DeleteKey (user + "crosswordscore" + counter);
			PlayerPrefs.Save ();
			counter++;
		}
		counter = 1;
		while(counter > 12){
			PlayerPrefs.DeleteKey (user + "memoryscore" + counter);
			PlayerPrefs.Save ();
			counter++;
		}
		counter = 1;
		while(counter > 12){
			PlayerPrefs.DeleteKey (user + "quizscore" + counter);
			PlayerPrefs.Save ();
			counter++;
		}
		PlayerPrefs.DeleteKey (user + "level");
		PlayerPrefs.Save ();
		PlayerPrefs.DeleteKey ("CurrentUser");
		PlayerPrefs.Save ();
		Application.LoadLevel (0);
	}

}

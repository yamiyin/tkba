﻿using UnityEngine;
using System.Collections;

public class StoryLevelManager : MonoBehaviour {

	// Use this for initialization

	public GameObject level1;
	public GameObject level2;
	public GameObject level3;
	public GameObject level4;
	public GameObject level5;
	public GameObject level6;
	public GameObject level7;
	public GameObject level8;
	public GameObject level9;
	public GameObject level10;
	public GameObject level11;
	public GameObject level12;

	public static int gameLevel = 0;
	private float gameTime = 60f;

	public int newLevelProgress;
	public int oldLevel;

	public string user;
	void Start ()
	{
		Crossword.gameFrom = "Story";
		user = PlayerPrefs.GetString ("CurrentUser");
		if (PlayerPrefs.HasKey (user + "level")) {
			oldLevel = PlayerPrefs.GetInt (user + "level"); 
			Debug.Log ("CurrentLevel: " + oldLevel);
		} 
		else {
			oldLevel = 1;
		}
	}

	void level1clicked () {
		Crossword.startTime = gameTime * .2f;
		gameLevel = 5;
		Application.LoadLevel (5);
	}

	void level2clicked () {
		Crossword.startTime = gameTime * 3f;
		gameLevel = 9;
		Application.LoadLevel (9);
	}

	void level3clicked () {
		Crossword.startTime = gameTime * 3f;
		gameLevel = 10;
		Application.LoadLevel (10);
	}

	void level4clicked () {
		Crossword.startTime = gameTime * 3f;
		gameLevel = 11;
		Application.LoadLevel (11);
	}

	void level5clicked () {
		Crossword.startTime = gameTime * 2f;
		gameLevel = 12;
		Application.LoadLevel (12);
	}

	void level6clicked () {
		Crossword.startTime = gameTime * 2f;
		gameLevel = 13;
		Application.LoadLevel (13);
	}

	void level7clicked () {
		Crossword.startTime = gameTime * 2f;
		gameLevel = 14;
		Application.LoadLevel (14);
	}

	void level8clicked () {
		Crossword.startTime = gameTime * 2f;
		gameLevel = 15;
		Application.LoadLevel (15);
	}

	void level9clicked () {
		Crossword.startTime = gameTime * 1.5f;
		gameLevel = 16;
		Application.LoadLevel (16);
	}

	void level10clicked () {
		Crossword.startTime = gameTime * 1.5f;
		Application.LoadLevel (6);
	}

	void level11clicked () {
		Crossword.startTime = gameTime * 1.5f;
		Application.LoadLevel (7);
	}

	void level12clicked () {
		Crossword.startTime = gameTime * 1.5f;
		Application.LoadLevel (8);
	}
	void backbuttonclicked () {
		Application.LoadLevel (56);
	}

	void instructionsbuttonclicked () {
		Application.LoadLevel (57);
	}



	void StoreLevelProgress2()
	{

		newLevelProgress = 2;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
			PlayerPrefs.Save();
	}
	void StoreLevelProgress3()
	{

		newLevelProgress = 3;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}
	void StoreLevelProgress4()
	{

		newLevelProgress = 4;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}

	void StoreLevelProgress5()
	{

		newLevelProgress = 5;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}
	void StoreLevelProgress6()
	{
		 
		newLevelProgress = 6;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}

	void StoreLevelProgress7()
	{

		newLevelProgress = 7;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}

	void StoreLevelProgress8()
	{

		newLevelProgress = 8;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}


	void StoreLevelProgress9()
	{

		newLevelProgress = 9;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}

	void StoreLevelProgress10()
	{
		 
		newLevelProgress = 10;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}

	void StoreLevelProgress11()
	{

		newLevelProgress = 11;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}

	void StoreLevelProgress12()
	{
		newLevelProgress = 12;
		if( newLevelProgress > oldLevel)
			PlayerPrefs.SetInt(user + "level", newLevelProgress);
		PlayerPrefs.Save();
	}
		
	void Update ()
	{
		if (PlayerPrefs.HasKey (user + "crosswordscore1") && PlayerPrefs.HasKey (user + "memoryscore1") && PlayerPrefs.HasKey (user + "quizscore1")) {
			PlayerPrefs.SetInt (user + "level", 1);
			PlayerPrefs.Save ();
			level2.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore2") && PlayerPrefs.HasKey (user + "memoryscore2") && PlayerPrefs.HasKey (user + "quizscore2")) {
			PlayerPrefs.SetInt (user + "level", 2);
			PlayerPrefs.Save ();
			level3.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore3") && PlayerPrefs.HasKey (user + "memoryscore3") && PlayerPrefs.HasKey (user + "quizscore3")) {
			PlayerPrefs.SetInt (user + "level", 3);
			PlayerPrefs.Save ();
			level4.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore4") && PlayerPrefs.HasKey (user + "memoryscore4") && PlayerPrefs.HasKey (user + "quizscore4")) {
			PlayerPrefs.SetInt (user + "level", 4);
			PlayerPrefs.Save ();
			level5.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore5") && PlayerPrefs.HasKey (user + "memoryscore5") && PlayerPrefs.HasKey (user + "quizscore5")) {
			PlayerPrefs.SetInt (user + "level", 5);
			PlayerPrefs.Save ();
			level6.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore6") && PlayerPrefs.HasKey (user + "memoryscore6") && PlayerPrefs.HasKey (user + "quizscore6")) {
			PlayerPrefs.SetInt (user + "level", 6);
			PlayerPrefs.Save ();
			level7.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore7") && PlayerPrefs.HasKey (user + "memoryscore7") && PlayerPrefs.HasKey (user + "quizscore7")) {
			PlayerPrefs.SetInt (user + "level", 7);
			PlayerPrefs.Save ();
			level8.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore8") && PlayerPrefs.HasKey (user + "memoryscore8") && PlayerPrefs.HasKey (user + "quizscore8")) {
			PlayerPrefs.SetInt (user + "level", 8);
			PlayerPrefs.Save ();
			level9.SetActive (true);
		}
	
		if (PlayerPrefs.HasKey (user + "crosswordscore9") && PlayerPrefs.HasKey (user + "memoryscore9") && PlayerPrefs.HasKey (user + "quizscore9")) {
			PlayerPrefs.SetInt (user + "level", 9);
			PlayerPrefs.Save ();
			level10.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore10") && PlayerPrefs.HasKey (user + "memoryscore10") && PlayerPrefs.HasKey (user + "quizscore10")) {
			PlayerPrefs.SetInt (user + "level", 10);
			PlayerPrefs.Save ();
			level11.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore11") && PlayerPrefs.HasKey (user + "memoryscore11") && PlayerPrefs.HasKey (user + "quizscore11")) {
			PlayerPrefs.SetInt (user + "level", 11);
			PlayerPrefs.Save ();
			level12.SetActive (true);
		}

		if (PlayerPrefs.HasKey (user + "crosswordscore12") && PlayerPrefs.HasKey (user + "memoryscore12") && PlayerPrefs.HasKey (user + "quizscore12")) {
			PlayerPrefs.SetInt (user + "level", 12);
			PlayerPrefs.Save ();
		}
	}
}

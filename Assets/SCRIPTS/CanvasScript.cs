﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasScript : MonoBehaviour {

	public Canvas iCanvas;

	public void Start(){
		
		string user = PlayerPrefs.GetString ("CurrentUser");

		if (user == null || !PlayerPrefs.HasKey ("CurrentUser")) {
			user = "Guest";
		}

		Debug.Log (user);

		Text[] textValue = iCanvas.GetComponentsInChildren<Text> ();
		textValue[3].text = user;

		if (PlayerPrefs.HasKey (user + "crosswordscore12") && PlayerPrefs.HasKey (user + "memoryscore12") && PlayerPrefs.HasKey (user + "quizscore12")) {
			Debug.Log ("Enabling Button");
			Button[] buttonValue = iCanvas.GetComponentsInChildren<Button> ();
			Debug.Log (buttonValue);
			buttonValue[2].interactable = true;
		}
	}

	public void MenuButtonClicked (int sceneId) {
		if (sceneId == 3) {
			PlayerPrefs.SetInt ("level", 13);
		}
		Debug.Log("Load " + sceneId);

		Application.LoadLevel (sceneId);

	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UserManager : MonoBehaviour {
	
	public InputField iField;
	public Canvas iCanvas;
	string myName;
	public GameObject prefabButton;
	public RectTransform ParentPanel;
	public Text err;

	//Use this for initialization
	void Start(){

		//for debbuging, clears all saved data
		//PlayerPrefs.DeleteAll ();

		PopulateUserList ();

		if (!PlayerPrefs.HasKey ("Users")) {
			iCanvas.gameObject.SetActive (true);
		}

	}

	public void CreateNewUser () {

		myName = iField.text.ToUpper();

		if (myName.Length < 2) {
			
		} 
		else {
			string userlist = PlayerPrefs.GetString ("Users").ToUpper();
			Debug.Log ("Username: " + myName);
			if (userlist.Contains (",")) {
				if (!userlist.Contains (myName + ",") && !userlist.Contains ("," + myName)) {
					if (!PlayerPrefs.HasKey ("Users")) {
				
						PlayerPrefs.SetString ("Users", myName);
						PlayerPrefs.Save ();

						iField.text = "";

						PopulateUserList ();

						iCanvas.gameObject.SetActive(false);


					} else {
				
						PlayerPrefs.SetString ("Users", PlayerPrefs.GetString ("Users") + "," + myName);
						PlayerPrefs.Save ();

						Debug.Log ("UserPrefs: " + PlayerPrefs.GetString ("Users"));

						iField.text = "";

						PopulateUserList ();

						iCanvas.gameObject.SetActive(false);
					}
				} else {
					err.text = "Player already exist!";
				}
			} else {
				Debug.Log (userlist);
				if (userlist.Contains (myName)) {
					Debug.Log ("Err2:" + userlist.Contains (myName) + "UserList:" + userlist);
					err.text = "Player already exist!";
				} else {
					if (!PlayerPrefs.HasKey ("Users")) {

						PlayerPrefs.SetString ("Users", myName);
						PlayerPrefs.Save ();

						iField.text = "";

						PopulateUserList ();

						iCanvas.gameObject.SetActive(false);

					} else {

						PlayerPrefs.SetString ("Users", PlayerPrefs.GetString ("Users") + "," + myName);
						PlayerPrefs.Save ();

						Debug.Log ("UserPrefs: " + PlayerPrefs.GetString ("Users"));

						iField.text = "";

						PopulateUserList ();

						iCanvas.gameObject.SetActive(false);
					}
				}
			}




		}
	}

	public void RedirectToHome(string user){
		PlayerPrefs.SetString ("CurrentUser", user);
		PlayerPrefs.Save();
		Application.LoadLevel (56);
	}

	public void PopulateUserList(){
		if (PlayerPrefs.HasKey ("Users")) {
			int btnid = 0;

			string[] user = PlayerPrefs.GetString ("Users").Split (',');

			for (int i = 0; i < ParentPanel.childCount; i++)
			{
				if (i != 0) {
					GameObject.Destroy (ParentPanel.GetChild (i).gameObject);
				}
			}

			foreach (string item in user) {
				
				bool activate = true;

				GameObject userbtn = (GameObject)Instantiate (prefabButton);

				userbtn.transform.SetParent (ParentPanel, false);

				userbtn.transform.localScale = new Vector3 (1, 1, 1);

				userbtn.gameObject.SetActive (true);

				userbtn.GetComponentInChildren<Text> ().text = item;
				Debug.Log ("Load data:" + item);
				string captured = item;
				userbtn.GetComponent<Button>().onClick.AddListener(() => RedirectToHome(captured));
				Debug.Log ("Load data success");
				Button tempButton = userbtn.GetComponent<Button> ();
			}
		}
	}
}

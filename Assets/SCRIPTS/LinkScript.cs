﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LinkScript : MonoBehaviour {

	public int stageLevel;
	public Canvas iCanvas;

	// Use this for initialization
	void Start () {
		string user = PlayerPrefs.GetString ("CurrentUser");
		PlayerPrefs.SetInt ("level", stageLevel);
		PlayerPrefs.Save();
		//int crosswordscore = PlayerPrefs.GetInt (user + "crosswordscore" + stageLevel);
		//int memoryscore = PlayerPrefs.GetInt (user + "memoryscore" + stageLevel);
		//int quizscore = PlayerPrefs.GetInt (user + "quizscore" + stageLevel);
		bool crosswordactive = false;
		bool memoryactive = false;
		bool quizactive = false;

		Debug.Log ("Current Level Access: " + stageLevel);
		Debug.Log ("Check Cross Data: " + PlayerPrefs.HasKey (user + "crosswordscore" + stageLevel));
		Debug.Log ("Check mem Data: " + PlayerPrefs.HasKey (user + "memoryscore" + stageLevel));
		Debug.Log ("Check quiz Data: " + PlayerPrefs.HasKey (user + "quizscore" + stageLevel));

		if (PlayerPrefs.HasKey (user + "crosswordscore" + stageLevel)) {
			crosswordactive = true;
		}
		if (PlayerPrefs.HasKey (user + "memoryscore" + stageLevel)) {
			memoryactive = true;
		}
		if (PlayerPrefs.HasKey (user + "quizscore" + stageLevel)) {
			quizactive = true;
		}
		Image[] img = iCanvas.GetComponentsInChildren<Image>();
		Debug.Log ("Star count:" + img.Length);
		if (!crosswordactive) {
			img [0].gameObject.SetActive (false);
		}
		if (!memoryactive) {
			img [1].gameObject.SetActive (false);
		}
		if (!quizactive) {
			img [2].gameObject.SetActive (false);
		}


	}
	
	// Update is called once per frame
	public void BackToLevelSelector () {
		Application.LoadLevel (17);
	}

	public void goToCrossword(int level){
		if (level < 5) {
			Crossword.startTime = 60f * 3;
		} 
		if (level < 9 && level > 4) {
			Crossword.startTime = 60f * 2;
		}
		if(level < 13 && level > 8){ 
			Crossword.startTime = 60f * 1;
		}
		Application.LoadLevel ("Crossword" + level);
	}

	public void goToMemory(int level){
		Application.LoadLevel ("game" + level);
	}
	public void goToQuiz(int level){
		Application.LoadLevel ("QuizLevel" + level);
	}


}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class QuizManager : MonoBehaviour {

	public Canvas iCanvas;
	public GameObject q1;
	public GameObject q2;
	public GameObject q3;
	public GameObject q4;
	public GameObject q5;
	public GameObject q6;
	public GameObject q7;
	public GameObject q8;
	public GameObject q9;
	public GameObject q10;
	public GameObject finsh;
	public GameObject fail;
	public AudioSource correct;
	public AudioSource wrong;


	// Use this for initialization
	string user;
	int level;
	int score = 0;
	int question = 1;
	void Start () {
		user = PlayerPrefs.GetString ("CurrentUser");
		level = PlayerPrefs.GetInt ("level");
		List<Button> btns = q1.GetComponentsInChildren<Button>().ToList();
		int startrand = btns.Count - 1;
		Vector3 pos = new Vector3 ();
		pos.y = -201;

	}

	public void answerClick (bool answer) {
		if (answer) {
			score++;
			question++;
			Text[] textValue = iCanvas.GetComponentsInChildren<Text> ();
			textValue [0].text = score.ToString ();
			if (question == 2) {
				q2.SetActive (true);
				q1.SetActive (false);
			}
			if (question == 3) {
				q3.SetActive (true);
				q2.SetActive (false);
			}
			if (question == 4) {
				q4.SetActive (true);
				q3.SetActive (false);
			}
			if (question == 5) {
				q5.SetActive (true);
				q4.SetActive (false);
			}
			if (question == 6) {
				q6.SetActive (true);
				q5.SetActive (false);
			}
			if (question == 7) {
				q7.SetActive (true);
				q6.SetActive (false);
			}
			if (question == 8) {
				q8.SetActive (true);
				q7.SetActive (false);
			}
			if (question == 9) {
				q9.SetActive (true);
				q8.SetActive (false);
			}
			if (question == 10) {
				q10.SetActive (true);
				q9.SetActive (false);
			}
			if (question == 11) {
				q10.SetActive (false);
				if (score >= 5) {
					PlayerPrefs.SetInt (user + "quizscore" + level, score);
					PlayerPrefs.Save();
					finsh.SetActive (true);
				} 
				else {
					fail.SetActive (true);
				}
			}
			correct.Play ();
		} else {
			question++;
			if (question == 2) {
				q2.SetActive (true);
				q1.SetActive (false);
			}
			if (question == 3) {
				q3.SetActive (true);
				q2.SetActive (false);
			}
			if (question == 4) {
				q4.SetActive (true);
				q3.SetActive (false);
			}
			if (question == 5) {
				q5.SetActive (true);
				q4.SetActive (false);
			}
			if (question == 6) {
				q6.SetActive (true);
				q5.SetActive (false);
			}
			if (question == 7) {
				q7.SetActive (true);
				q6.SetActive (false);
			}
			if (question == 8) {
				q8.SetActive (true);
				q7.SetActive (false);
			}
			if (question == 9) {
				q9.SetActive (true);
				q8.SetActive (false);
			}
			if (question == 10) {
				q10.SetActive (true);
				q9.SetActive (false);
			}
			if (question == 11) {
				q10.SetActive (false);
				if (score >= 5) {
					PlayerPrefs.SetInt (user + "quizscore" + level, score);
					PlayerPrefs.Save();
					finsh.SetActive (true);
				} 
				else {
					fail.SetActive (true);
				}
			}
			wrong.Play ();
		}
	}

	public void BacktoGameSelect(){
		if (level == 13) {
			Application.LoadLevel (30);
		} else {
			Application.LoadLevel ("Story" + level);
		}
	}
		
}

static class MyExtensions{
	private static System.Random rng = new System.Random();  

	public static void ShuffleBtn<T>(this IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}
}

﻿using UnityEngine;
using System.Collections;

public class QuestLevels : MonoBehaviour {
	public GUISkin skin;
	public int oldHighscore;
	// Use this for initialization
	void Start () {
		string user = PlayerPrefs.GetString ("CurrentUser");
		oldHighscore = PlayerPrefs.GetInt(user + "level");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		if(skin != null)
			GUI.skin = skin;
		
		GUI.BeginGroup(new Rect(5, 5, 160, Screen.height - 10));
		{
			GUI.Box(new Rect(0, 0, 160, Screen.height - 10), "");
			
			// Play clears the Game Field and Deals a new set of Cards
			if(GUI.Button(new Rect(5, 5, 150, 48), "Quiz 1") )
			{
				//
				Application.LoadLevel("QuizLevel1");
			}
			
			if ( oldHighscore >= 2){if(GUI.Button(new Rect(5, 55, 150, 48), "Quiz 2") )
			{
				//
				Application.LoadLevel("QuizLevel2");
				}}
			if ( oldHighscore >= 3){if(GUI.Button(new Rect(5, 105, 150, 48), "Quiz 3")) 
			{
				//
				Application.LoadLevel("QuizLevel3");
				}}
			
			if ( oldHighscore >= 4){if(GUI.Button(new Rect(5, 155, 150, 48), "Quiz 4") )
			{
				//
				Application.LoadLevel("QuizLevel4");
				}}
			
			if ( oldHighscore >= 5){if(GUI.Button(new Rect(5, 205, 150, 48), "Quiz 5"))
			{
				//
				Application.LoadLevel("QuizLevel5");
				}}
			
			if ( oldHighscore >= 6){if(GUI.Button(new Rect(5, 255, 150, 48), "Quiz 6") )
			{
				//
				Application.LoadLevel("QuizLevel6");
				}}
			
			if ( oldHighscore >= 7){if(GUI.Button(new Rect(5, 305, 150, 48), "Quiz 7") )
			{
				//
				Application.LoadLevel("QuizLevel7");
				}}
			
			if ( oldHighscore >= 8){if(GUI.Button(new Rect(5, 355, 150, 48), "Quiz 8") )
			{
				//
				Application.LoadLevel("QuizLevel8");
				}}
			
			if ( oldHighscore >= 9){if(GUI.Button(new Rect(5, 405, 150, 48), "Quiz 9") )
			{
				//
				Application.LoadLevel("QuizLevel9");
				}}
			
			if ( oldHighscore >= 10){if(GUI.Button(new Rect(5, 455, 150, 48), "Quiz 10"))
			{
				//
				Application.LoadLevel("QuizLevel10");
				}}
			
			if ( oldHighscore >= 11){if(GUI.Button(new Rect(5, 505, 150, 48), "Quiz 11") )
			{
				//
				Application.LoadLevel("QuizLevel11");
				}}
			
			if ( oldHighscore >= 12){if(GUI.Button(new Rect(5, 555, 150, 48), "Quiz 12"))
			{
				//
				Application.LoadLevel("QuizLevel12");
				}}
			if(GUI.Button(new Rect(5, 605, 150, 48), " Home"))
			{
				//
				Application.LoadLevel(56);
			}
			
			
			
			
			//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//	GUI.skin.label.fontSize = 16;
			//	GUI.Label(new Rect(16, 80, 128, 32), "Pairs " + pairCount);
			
			//	pairCount = (int)GUI.HorizontalSlider(new Rect(16, 112, 128, 32), pairCount, 2, spriteCardsFront.Length);
			
			//	if(pairCount != oldPairCount)
			//	{
			//		oldPairCount = pairCount;
			
			//		bestMoves = PlayerPrefs.GetInt("Memory_" + pairCount + "_Pairs", 0);
			//	}
			
			//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//	GUI.skin.label.fontSize = 16;
			//	GUI.Label(new Rect(16, 160, 128, 32), "Moves: " + totalMoves);
			
			//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//	GUI.skin.label.fontSize = 16;
			//	GUI.Label(new Rect(16, 192, 128, 32), "Best: " + bestMoves);
			
		}
		GUI.EndGroup();
	}
}

﻿using UnityEngine;
using System.Collections;

public class CrosswordLevelManager : MonoBehaviour {
	public GUISkin skin;
	public int oldHighscore;

	public int gameLevel = 0;
	private float gameTime = 60f;
	// Use this for initialization
	void Start () {
		Crossword.gameFrom = "Crossword";
		string user = PlayerPrefs.GetString ("CurrentUser");
		int level = PlayerPrefs.GetInt ("level");
		oldHighscore = PlayerPrefs.GetInt(user + "level");
		Debug.Log ("Current Level Cross:" + level);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI()
	{
		if(skin != null)
			GUI.skin = skin;
		
		GUI.BeginGroup(new Rect(5, 5, 160, Screen.height - 10));
		{
			GUI.Box(new Rect(0, 0, 160, Screen.height - 10), "");
			
			// Play clears the Game Field and Deals a new set of Cards
			if(GUI.Button(new Rect(5, 5, 150, 48), "Crossword 1") )
			{
				//
				Crossword.startTime = gameTime * 3;
				Application.LoadLevel("Crossword1");
			}
			
			if ( oldHighscore >= 2){if(GUI.Button(new Rect(5, 55, 150, 48), "Crossword 2") )
			{
				//
				Crossword.startTime = gameTime * 3;
				Application.LoadLevel("Crossword2");
				}}
			if ( oldHighscore >= 3){if(GUI.Button(new Rect(5, 105, 150, 48), "Crossword 3")) 
			{
				//
				Crossword.startTime = gameTime * 3;
				Application.LoadLevel("Crossword3");
				}}
			
			if ( oldHighscore >= 4){if(GUI.Button(new Rect(5, 155, 150, 48), "Crossword 4") )
			{
				//
				Crossword.startTime = gameTime * 3;
				Application.LoadLevel("Crossword4");
				}}
			
			if ( oldHighscore >= 5){if(GUI.Button(new Rect(5, 205, 150, 48), "Crossword 5"))
			{
				//
				Crossword.startTime = gameTime * 2;
				Application.LoadLevel("Crossword5");
				}}
			
			if ( oldHighscore >= 6){if(GUI.Button(new Rect(5, 255, 150, 48), "Crossword 6") )
			{
				//
				Crossword.startTime = gameTime * 2;
				Application.LoadLevel("Crossword6");
				}}
			
			if ( oldHighscore >= 7){if(GUI.Button(new Rect(5, 305, 150, 48), "Crossword 7") )
			{
				//
				Crossword.startTime = gameTime * 2;
				Application.LoadLevel("Crossword7");
				}}
			
			if ( oldHighscore >= 8){if(GUI.Button(new Rect(5, 355, 150, 48), "Crossword 8") )
			{
				//
					Crossword.startTime = gameTime * 2;
				Application.LoadLevel("Crossword8");
				}}
			
			if ( oldHighscore >= 9){if(GUI.Button(new Rect(5, 405, 150, 48), "Crossword 9") )
			{
				//
					Crossword.startTime = gameTime * 1.5f;
				Application.LoadLevel("Crossword9");
				}}
			
			if ( oldHighscore >= 10){if(GUI.Button(new Rect(5, 455, 150, 48), "Crossword 10"))
			{
				//
					Crossword.startTime = gameTime * 1.5f;
				Application.LoadLevel("Crossword10");
				}}
			
			if ( oldHighscore >= 11){if(GUI.Button(new Rect(5, 505, 150, 48), "Crossword 11") )
			{
				//
					Crossword.startTime = gameTime * 1.5f;
				Application.LoadLevel("Crossword11");
				}}
			
			if ( oldHighscore >= 12){if(GUI.Button(new Rect(5, 555, 150, 48), "Crossword 12"))
			{
				//
					Crossword.startTime = gameTime * 1.5f;
				Application.LoadLevel("Crossword12");
				}}
			if(GUI.Button(new Rect(5, 605, 150, 48), " Home"))
			{
				//
				Application.LoadLevel(56);
			}
			
			
			
			
			//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//	GUI.skin.label.fontSize = 16;
			//	GUI.Label(new Rect(16, 80, 128, 32), "Pairs " + pairCount);
			
			//	pairCount = (int)GUI.HorizontalSlider(new Rect(16, 112, 128, 32), pairCount, 2, spriteCardsFront.Length);
			
			//	if(pairCount != oldPairCount)
			//	{
			//		oldPairCount = pairCount;
			
			//		bestMoves = PlayerPrefs.GetInt("Memory_" + pairCount + "_Pairs", 0);
			//	}
			
			//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//	GUI.skin.label.fontSize = 16;
			//	GUI.Label(new Rect(16, 160, 128, 32), "Moves: " + totalMoves);
			
			//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//	GUI.skin.label.fontSize = 16;
			//	GUI.Label(new Rect(16, 192, 128, 32), "Best: " + bestMoves);
			
		}
		GUI.EndGroup();
	}
}

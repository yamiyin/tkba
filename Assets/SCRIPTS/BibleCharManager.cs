﻿using UnityEngine;
using System.Collections;

public class BibleCharManager : MonoBehaviour {
	public GameObject bc1;
	public GameObject bc2;
	public GameObject bc3;
	public GameObject bc4;
	public GameObject bc5;
	public GameObject bc6;
	public GameObject bc7;
	public GameObject bc8;
	public GameObject bc9;
	public GameObject bc10;
	public GameObject bc11;
	public GameObject bc12;


	public GameObject p1;
	public GameObject p2;
	public GameObject p3;
	public GameObject p4;
	public GameObject p5;
	public GameObject p6;
	public GameObject p7;
	public GameObject p8;
	public GameObject p9;
	public GameObject p10;
	public GameObject p11;
	public GameObject p12;


	public GameObject cl1;
	public GameObject cl2;
	public GameObject cl3;
	public GameObject cl4;
	public GameObject cl5;
	public GameObject cl6;
	public GameObject cl7;
	public GameObject cl8;
	public GameObject cl9;
	public GameObject cl10;
	public GameObject cl11;
	public GameObject cl12;
	public int oldHighscore;
	public int chars=1;

	public int reset=1;
	public int reset1=12;
	string user;
	// Use this for initialization
	void Start () {
		user = PlayerPrefs.GetString ("CurrentUser");
		if (PlayerPrefs.HasKey (user + "level")) {
			oldHighscore = PlayerPrefs.GetInt (user + "level"); 
			Debug.Log ("CurrentLevel: " + oldHighscore);
		} 
		else {
			oldHighscore = 1;
		}
		CheckLevel ();
	}


	void profclose(){
		p1.SetActive (false);
		p2.SetActive (false);
		p3.SetActive (false);
		p4.SetActive (false);
		p5.SetActive (false);
		p6.SetActive (false);
		p7.SetActive (false);
		p8.SetActive (false);
		p9.SetActive (false);
		p10.SetActive (false);
		p11.SetActive (false);
		p12.SetActive (false);

	
	
	}

	void CheckLevel ()
	{
		if (PlayerPrefs.HasKey(user + "crosswordscore1") && PlayerPrefs.HasKey(user + "memoryscore1") && PlayerPrefs.HasKey(user + "quizscore1"))
			PlayerPrefs.SetInt (user + "level", 1);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore2") && PlayerPrefs.HasKey(user + "memoryscore2") && PlayerPrefs.HasKey(user + "quizscore2"))
			PlayerPrefs.SetInt (user + "level", 2);
			PlayerPrefs.Save ();


		if (PlayerPrefs.HasKey(user + "crosswordscore3") && PlayerPrefs.HasKey(user + "memoryscore3") && PlayerPrefs.HasKey(user + "quizscore3"))
			PlayerPrefs.SetInt (user + "level", 3);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore4") && PlayerPrefs.HasKey(user + "memoryscore4") && PlayerPrefs.HasKey(user + "quizscore4"))
			PlayerPrefs.SetInt (user + "level", 4);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore5") && PlayerPrefs.HasKey(user + "memoryscore5") && PlayerPrefs.HasKey(user + "quizscore5"))
			PlayerPrefs.SetInt (user + "level", 5);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore6") && PlayerPrefs.HasKey(user + "memoryscore6") && PlayerPrefs.HasKey(user + "quizscore6"))
			PlayerPrefs.SetInt (user + "level", 6);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore7") && PlayerPrefs.HasKey(user + "memoryscore7") && PlayerPrefs.HasKey(user + "quizscore7"))
			PlayerPrefs.SetInt (user + "level", 7);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore8") && PlayerPrefs.HasKey(user + "memoryscore8") && PlayerPrefs.HasKey(user + "quizscore8"))
			PlayerPrefs.SetInt (user + "level", 8);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore9") && PlayerPrefs.HasKey(user + "memoryscore9") && PlayerPrefs.HasKey(user + "quizscore9"))
			PlayerPrefs.SetInt (user + "level", 9);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore10") && PlayerPrefs.HasKey(user + "memoryscore10") && PlayerPrefs.HasKey(user + "quizscore10"))
			PlayerPrefs.SetInt (user + "level", 10);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore11") && PlayerPrefs.HasKey(user + "memoryscore11") && PlayerPrefs.HasKey(user + "quizscore11"))
			PlayerPrefs.SetInt (user + "level", 11);
			PlayerPrefs.Save ();

		if (PlayerPrefs.HasKey(user + "crosswordscore12") && PlayerPrefs.HasKey(user + "memoryscore12") && PlayerPrefs.HasKey(user + "quizscore12"))
			PlayerPrefs.SetInt (user + "level", 12);
			PlayerPrefs.Save ();
	}

	void homeclick(){
		Application.LoadLevel(56);}
	void maryclick(){
		p1.SetActive (true);}

	void josephclick(){
		p2.SetActive (true);}

	void wisemenclick(){
		p3.SetActive (true);}

	void kingherodclick(){
		p4.SetActive (true);}

	void johnclick(){
		p5.SetActive (true);}

	void jesusclick(){
		p6.SetActive (true);}

	void peterclick(){
		p7.SetActive (true);}

	void jamesclick(){
		p8.SetActive (true);}

	void elijahclick(){
		p9.SetActive (true);}

	void judasclick(){
		p10.SetActive (true);}

	void pontiusclick(){
		p11.SetActive (true);}

	void marymagclick(){
		p12.SetActive (true);}


	void rightbutton() {
		
		chars++;
		Debug.Log (" " + chars);
		if (chars == 13) {
			
			chars=reset;
			
			Debug.Log ("reset to " + chars);
			cl12.SetActive (false);
		}
		
		
	

	}

	void leftbutton() {
		
		chars--;

		if (chars == 0) {
			
			chars=reset1;
			
			Debug.Log ("reset to " + chars);
		}

	}

	// Update is called once per frame
	void Update () {


		if (chars==1 ){
			bc1.SetActive (true);
			bc2.SetActive (false);
			bc3.SetActive (false);
			bc4.SetActive (false);
			bc5.SetActive (false);
			bc6.SetActive (false);
			bc7.SetActive (false);
			bc8.SetActive (false);
			bc9.SetActive (false);
			bc10.SetActive (false);
			bc11.SetActive (false);
			bc12.SetActive (false);            
			if (chars>oldHighscore){
				cl1.SetActive (true);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);

			}
		
		
		}

			                if (chars==2 ){
				bc1.SetActive (false);
				bc2.SetActive (true);
				bc3.SetActive (false);
				bc4.SetActive (false);
				bc5.SetActive (false);
				bc6.SetActive (false);
				bc7.SetActive (false);
				bc8.SetActive (false);
				bc9.SetActive (false);
				bc10.SetActive (false);
				bc11.SetActive (false);
				                bc12.SetActive (false);            
				                
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (true);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


				                if (chars==3 ){
					bc1.SetActive (false);
					bc2.SetActive (false);
					bc3.SetActive (true);
					bc4.SetActive (false);
					bc5.SetActive (false);
					bc6.SetActive (false);
					bc7.SetActive (false);
					bc8.SetActive (false);
					bc9.SetActive (false);
					bc10.SetActive (false);
					bc11.SetActive (false);
					                bc12.SetActive (false);            
					                
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (true);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


					                if (chars==4 ){
						bc1.SetActive (false);
						bc2.SetActive (false);
						bc3.SetActive (false);
						bc4.SetActive (true);
						bc5.SetActive (false);
						bc6.SetActive (false);
						bc7.SetActive (false);
						bc8.SetActive (false);
						bc9.SetActive (false);
						bc10.SetActive (false);
						bc11.SetActive (false);
						                bc12.SetActive (false);            
						                
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (true);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


						                if (chars==5 ){
			bc1.SetActive (false);
							bc2.SetActive (false);
							bc3.SetActive (false);
							bc4.SetActive (false);
							bc5.SetActive (true);
							bc6.SetActive (false);
							bc7.SetActive (false);
							bc8.SetActive (false);
							bc9.SetActive (false);
							bc10.SetActive (false);
							bc11.SetActive (false);
							                bc12.SetActive (false);            
							               
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (true);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


							                if (chars==6 ){
			bc1.SetActive (false);
								bc2.SetActive (false);
								bc3.SetActive (false);
								bc4.SetActive (false);
								bc5.SetActive (false);
								bc6.SetActive (true);
								bc7.SetActive (false);
								bc8.SetActive (false);
								bc9.SetActive (false);
								bc10.SetActive (false);
								bc11.SetActive (false);
								                bc12.SetActive (false);            
								               
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (true);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


								                if (chars==7 ){
			bc1.SetActive (false);
									bc2.SetActive (false);
									bc3.SetActive (false);
									bc4.SetActive (false);
									bc5.SetActive (false);
									bc6.SetActive (false);
									bc7.SetActive (true);
									bc8.SetActive (false);
									bc9.SetActive (false);
									bc10.SetActive (false);
									bc11.SetActive (false);
									                bc12.SetActive (false);            
									                
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (true);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


									                if (chars==8 ){
			bc1.SetActive (false);
										bc2.SetActive (false);
										bc3.SetActive (false);
										bc4.SetActive (false);
										bc5.SetActive (false);
										bc6.SetActive (false);
										bc7.SetActive (false);
										bc8.SetActive (true);
										bc9.SetActive (false);
										bc10.SetActive (false);
										bc11.SetActive (false);
										                bc12.SetActive (false);            
										               
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (true);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}
		}


										                if (chars==9 ){
			bc1.SetActive (false);
											bc2.SetActive (false);
											bc3.SetActive (false);
											bc4.SetActive (false);
											bc5.SetActive (false);
											bc6.SetActive (false);
											bc7.SetActive (false);
											bc8.SetActive (false);
											bc9.SetActive (true);
											bc10.SetActive (false);
											bc11.SetActive (false);
											                bc12.SetActive (false);            
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (true);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}									                
		}


											                if (chars==10 ){
			bc1.SetActive (false);
												bc2.SetActive (false);
												bc3.SetActive (false);
												bc4.SetActive (false);
												bc5.SetActive (false);
												bc6.SetActive (false);
												bc7.SetActive (false);
												bc8.SetActive (false);
												bc9.SetActive (false);
												bc10.SetActive (true);
												bc11.SetActive (false);
												                bc12.SetActive (false);            
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (true);
				cl11.SetActive (false);
				cl12.SetActive (false);
			}										                
		}


												                if (chars==11 ){
			bc1.SetActive (false);
													bc2.SetActive (false);
													bc3.SetActive (false);
													bc4.SetActive (false);
													bc5.SetActive (false);
													bc6.SetActive (false);
													bc7.SetActive (false);
													bc8.SetActive (false);
													bc9.SetActive (false);
													bc10.SetActive (false);
													bc11.SetActive (true);
													                bc12.SetActive (false);            
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (true);
				cl12.SetActive (false);
			}											                
		}

													                if (chars==12 ){
			bc1.SetActive (false);
														bc2.SetActive (false);
														bc3.SetActive (false);
														bc4.SetActive (false);
														bc5.SetActive (false);
														bc6.SetActive (false);
														bc7.SetActive (false);
														bc8.SetActive (false);
														bc9.SetActive (false);
														bc10.SetActive (false);
														bc11.SetActive (false);
														                bc12.SetActive (true);            
			if (chars>oldHighscore){
				cl1.SetActive (false);
				cl2.SetActive (false);
				cl3.SetActive (false);
				cl4.SetActive (false);
				cl5.SetActive (false);
				cl6.SetActive (false);
				cl7.SetActive (false);
				cl8.SetActive (false);
				cl9.SetActive (false);
				cl10.SetActive (false);
				cl11.SetActive (false);
				cl12.SetActive (true);
			}												                
		}







	}
}



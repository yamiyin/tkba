using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Crossword : MonoBehaviour {
	public GUISkin Myskin;
    public bool useDictionary;                      
    public TextAsset dictionary;                    
    public string[] words;                          
    public int maxWordsNumber;                     
    public int maxWordLength;                     
    public bool hint, allowInverse;                 
    public int horizontal, vertical;               
    public float sensibility;                      
    public float spacing;                           
    public GameObject tile, background, current;             
    public Color defaultColor, selectedColor, correctColor;
    [HideInInspector]
    public bool ready = false, correct = false;
    [HideInInspector]
    public string selectedWord = "";
    [HideInInspector]
    public List<GameObject> selected = new List<GameObject>();
	public GUISkin wor;
    private List<GameObject> tiles = new List<GameObject>();
    private GameObject temp, backgroundObj;
    private int solved = 0;
    public static float startTime, endTime;
    private string[,] wordMatrix;
    private Dictionary<string, bool> _words = new Dictionary<string, bool>();
    private Dictionary<string, bool> placedWords = new Dictionary<string, bool>();
    private string[] letters = new string[26] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "z", "x", "y", "w" };
    private Ray ray;
    private RaycastHit hit;
    private int marker = 0;

	Boolean isStop;
	public static string gameFrom;

    private static Crossword instance;
    public static Crossword Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
		Time.timeScale = 1;

        List<string> fetchLength = new List<string>();
        int counter = 0;

        if (useDictionary)
        {
            words = dictionary.text.Split(';');
        }
        else
        {
            maxWordsNumber = words.Length;
        }

        if (maxWordsNumber <= 0)
        {
            maxWordsNumber = 1;
        }
			

        Shuffle(words);

        Mathf.Clamp(maxWordLength, 0, vertical < horizontal ? horizontal : vertical);
       
       while (fetchLength.Count < maxWordsNumber + 1)
       {
         if (words[counter].Length <= maxWordLength)
           {
               fetchLength.Add(words[counter]);
           }
          counter++;
       }

       for (int i = 0; i < maxWordsNumber; i++)
       {
           if (!_words.ContainsKey(fetchLength[i].ToUpper()) && !_words.ContainsKey(fetchLength[i]))
           {	
				Debug.Log (i);
				Debug.Log (_words);
				if (hint) {
					_words.Add (fetchLength [i].ToUpper (), false);
					Debug.Log (fetchLength [i].ToUpper());
				} else {
					_words.Add (fetchLength [i], false);
					Debug.Log (fetchLength [i]);
				}	
           }

       }

		Debug.Log ("maxword:"+maxWordsNumber);

        Mathf.Clamp01(sensibility);

        wordMatrix = new string[horizontal, vertical];

        
        InstantiateBackground();

        
        for (int i = 0; i < horizontal; i++)
        {
            for (int j = 0; j < vertical; j++)
            {
                temp = Instantiate(tile, new Vector3(i * 1 * tile.transform.localScale.x * spacing, 10, j * 1 * tile.transform.localScale.z * spacing), Quaternion.identity) as GameObject;
                temp.name = "tile_" + i.ToString() + "_" + j.ToString();
				temp.transform.eulerAngles = new Vector3(180, 0, 0);
                temp.transform.parent = backgroundObj.transform;
                BoxCollider boxCollider = temp.GetComponent<BoxCollider>() as BoxCollider;
                boxCollider.size = new Vector3(sensibility, 1, sensibility);
                temp.GetComponent<Letter>().letter.text = "";
                temp.GetComponent<Letter>().horizontal = i;
                temp.GetComponent<Letter>().vertical = j;
                tiles.Add(tile);
                wordMatrix[i, j] = "";
            }
        }

        
        CenterBackground();

        PlaceWords();
        FillRest();

    }

    
    private void CenterBackground()
    {
		backgroundObj.transform.position = Camera.main.ScreenToWorldPoint(new Vector3((float)(Screen.width / 1.58), (float)(Screen.height / 1.8), 100));
    }

    private void InstantiateBackground()
    {
        if (horizontal % 2 != 0 && vertical % 2 == 0)
            backgroundObj = Instantiate(background, new Vector3((tile.transform.localScale.x * spacing) * (horizontal / 2), 1, (tile.transform.localScale.z * spacing) * (vertical / 2) - (tile.transform.localScale.z * spacing)), Quaternion.identity) as GameObject;
        else if (horizontal % 2 == 0 && vertical % 2 != 0)
            backgroundObj = Instantiate(background, new Vector3((tile.transform.localScale.x * spacing) * (horizontal / 2) - (tile.transform.localScale.x * spacing), 1, (tile.transform.localScale.z * spacing) * (vertical / 2)), Quaternion.identity) as GameObject;
        else
            backgroundObj = Instantiate(background, new Vector3((tile.transform.localScale.x * spacing) * (horizontal / 2) - (tile.transform.localScale.x * spacing), 1, (tile.transform.localScale.z * spacing) * (vertical / 2) - (tile.transform.localScale.z * spacing)), Quaternion.identity) as GameObject;

        backgroundObj.transform.eulerAngles = new Vector3(180, 0, 0);
        backgroundObj.transform.localScale = new Vector3(((tile.transform.localScale.x * spacing) * horizontal), 1, ((tile.transform.localScale.x * spacing) * vertical));
   }

    void Update()
    {
        
        if (Input.GetMouseButton(0) )
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit))
            {
               
                current = hit.transform.gameObject;
            }
            ready = true;
        }

        
        if (Input.GetMouseButtonUp(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit))
            {
                
                current = hit.transform.gameObject;
            }
            Check();
        }

       
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    ready = true;
                }
            }

            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Ended)
                {
                    Check();
                }
            }
        }
    }

    
    private void Check()
    {
        if (!correct)
        {
            foreach (KeyValuePair<string, bool> p in placedWords)
            {
                if (selectedWord.ToLower() == p.Key.Trim().ToLower())
                {
                    foreach (GameObject g in selected)
                    {
                        g.GetComponent<Letter>().solved = true;
                    }
                    correct = true;
                }
                if (allowInverse)
                {
                    if (Inverse(selectedWord.ToLower()) == p.Key.Trim().ToLower())
                    {
                        foreach (GameObject g in selected)
                        {
                            g.GetComponent<Letter>().solved = true;
                        }
                        correct = true;
                    }
                }
            }
        }

        if (correct)
        {
            placedWords.Remove(selectedWord);
            placedWords.Remove(Inverse(selectedWord));

            if (_words.ContainsKey(selectedWord))
                placedWords.Add(selectedWord, true);
            else if (_words.ContainsKey(Inverse(selectedWord)))
                placedWords.Add(Inverse(selectedWord), true);

            solved++;
        }

        if (solved == placedWords.Count)
            endTime = Time.timeSinceLevelLoad;

        ready = false;
        selected.Clear();
        selectedWord = "";
        correct = false;
    }

    private void PlaceWords()
    {
        System.Random rn = new System.Random();

        foreach (KeyValuePair<string, bool> p in _words)
        {

			Debug.Log (p);
            string s = p.Key.Trim();
            bool placed = false;
            
            while (placed == false)
            {
                
                int row = rn.Next(horizontal);
                int column = rn.Next(vertical);
                
                int dirX = 0;
                int dirY = 0;
                while (dirX == 0 && dirY == 0)
                {
                    dirX = rn.Next(3) - 1;
                    dirY = rn.Next(3) - 1;
                }
               
                if (hint)
                {
                    placed = PlaceWord(s.ToUpper(), row, column, dirX, dirY);
                }
                else
                {
                    placed = PlaceWord(s.ToLower(), row, column, dirX, dirY);
                }
                marker++;
                if (marker > 500)
                {
                    break;
                }
            }
        }
    }

    private bool PlaceWord(string word, int row, int col, int dirx, int diry)
    {
        
        if (dirx > 0)
        {
            if (row + word.Length >= horizontal)
            {
                return false;
            }
        }
        if (dirx < 0)
        {
            if (row - word.Length < 0)
            {
                return false;
            }
        }
        if (diry > 0)
        {
            if (col + word.Length >= vertical)
            {
                return false;
            }
        }
        if (diry < 0)
        {
            if (col - word.Length < 0)
            {
                return false;
            }
        }

        if (((0 * diry) + col) == vertical - 1)
            return false;

       
        for (int i = 0; i < word.Length; i++)
        {
            if (!string.IsNullOrEmpty(wordMatrix[(i * dirx) + row, (i * diry) + col]))
                return false;
        }

        
        placedWords.Add(word, false);
        char[] _w = word.ToCharArray();
        for (int i = 0; i < _w.Length; i++)
        {
            wordMatrix[(i * dirx) + row, (i * diry) + col] = _w[i].ToString();
            GameObject.Find("tile_" + ((i * dirx) + row).ToString() + "_" + ((i * diry) + col).ToString()).GetComponent<Letter>().letter.text = _w[i].ToString();
        }

        return true;
    }

    
    private void FillRest()
    {
        for (int i = 0; i < horizontal; i++)
        {
            for (int j = 0; j < vertical; j++)
            {
                if (wordMatrix[i, j] == "")
                {
                    wordMatrix[i, j] = letters[UnityEngine.Random.Range(0, letters.Length)];
                    GameObject.Find("tile_" + i.ToString() + "_" + j.ToString()).GetComponent<Letter>().letter.text = wordMatrix[i, j];
                }
            }
        }
    }

    
    private void Shuffle(string[] words)
    {
        for (int t = 0; t < words.Length; t++)
        {
            string tmp = words[t];
            int r = UnityEngine.Random.Range(t, words.Length);
			words[t] = words[r];
			words[r] = tmp;
        }
    }

    
    private string CurrentTime()
    {
		if (isStop) {
			return "00:00";
		} else {
			TimeSpan t = TimeSpan.FromSeconds (Mathf.RoundToInt (startTime - Time.timeSinceLevelLoad));
			return String.Format ("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
		}
    }

    
    private string FinalTime()
    {
		TimeSpan t = TimeSpan.FromSeconds(Mathf.RoundToInt(startTime - endTime));
        return String.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
    }

    
    private string Inverse(string word)
    {
        string result = "";
        char[] letters = word.ToCharArray();
        for (int i = letters.Length - 1; i >= 0; i--)
        {
            result += letters[i];
        }
        return result;
    }

    void OnGUI()
    {

        GUILayout.BeginVertical();
		int level = Application.loadedLevel - 31;
		if(GUI.Button(new Rect((Screen.width / 6) - 100, (Screen.height / 2) + 200,Screen.width/4,Screen.height/16),"Back To Game Menu"))
		{	
			if (PlayerPrefs.GetInt ("level") == 13) {
				Application.LoadLevel (31);
			} else {
				Application.LoadLevel ("Story" + PlayerPrefs.GetInt ("level"));
			}
		}
        if (solved == placedWords.Count)
        {
			Font ComicFont = (Font)Resources.Load ("COMICS");

			GUIStyle TimeStyle = new GUIStyle ();
			TimeStyle.normal.textColor = Color.black;
			TimeStyle.padding.left = 90;	
			TimeStyle.font = ComicFont;
			TimeStyle.padding.top = 50;
			TimeStyle.fontSize = 60;

			GUIStyle CountDownStyle = new GUIStyle ();
			CountDownStyle.normal.textColor = Color.black;
			CountDownStyle.fontSize = 60;
			CountDownStyle.padding.top = 50;
			CountDownStyle.font = ComicFont;

			GUI.Box(new Rect(Screen.width*1/4,Screen.height*2/6,Screen.width*2/4,Screen.height*1/6), "Solved!");
            GUILayout.BeginHorizontal();
			GUILayout.Label("Time:", TimeStyle);
			GUILayout.Label(FinalTime(), CountDownStyle);
            GUILayout.EndHorizontal();
			string user = PlayerPrefs.GetString ("CurrentUser");
			Debug.Log ("Current Level Finish Cross:" + level);
			PlayerPrefs.SetInt (user + "crosswordscore" + level, level);
			PlayerPrefs.Save();
			if(GUI.Button(new Rect(Screen.width*1/4,Screen.height*2/6,Screen.width*2/4,Screen.height*1/6),"Done"))
			{	
				Application.LoadLevel("Story" + level);
			}
        }
        else
        {

			Font ComicFont = (Font)Resources.Load ("COMICS");

			GUIStyle TimeStyle = new GUIStyle ();
			TimeStyle.normal.textColor = Color.black;
			TimeStyle.padding.left = 90;	
			TimeStyle.font = ComicFont;
			TimeStyle.padding.top = 50;
			TimeStyle.fontSize = 60;

			GUIStyle CountDownStyle = new GUIStyle ();
			CountDownStyle.normal.textColor = Color.black;
			CountDownStyle.fontSize = 60;
			CountDownStyle.padding.top = 50;
			CountDownStyle.font = ComicFont;


			GUIStyle gUIStyle2 = new GUIStyle ();
			gUIStyle2.normal.textColor = Color.white;
			gUIStyle2.alignment = TextAnchor.UpperCenter;
			gUIStyle2.fontSize = 60;

			GUILayout.BeginHorizontal ();
			GUILayout.Label ("Time ", TimeStyle);
			GUILayout.Label (CurrentTime (), CountDownStyle);
			GUILayout.EndHorizontal ();

			if (CurrentTime () == "00:00") {
				
				isStop = true;
				GUI.skin = Myskin;
				GUI.Box(new Rect(0,0,Screen.width,Screen.height),"Time's Up?");
				GUI.Label(new Rect(Screen.width*1/4,Screen.height*2/6,Screen.width*2/4,Screen.height*1/6), "", gUIStyle2);
				if(GUI.Button(new Rect(Screen.width*1/4,Screen.height*2/6,Screen.width*2/4,Screen.height*1/6),"Try Again"))
				{	
					if (gameFrom == "Crossword") {
						Application.LoadLevel ("CrosswordLevelManager");
					} else {
						if (level < 5) {
							Crossword.startTime = 60f * 3;
						} 
						if (level < 9 && level > 4) {
							Crossword.startTime = 60f * 2;
						}
						if(level < 13 && level > 8){ 
							Crossword.startTime = 60f * 1;
						}
						Application.LoadLevel("Crossword" + level);
					}
				}
					

				if(GUI.Button(new Rect(Screen.width/4,Screen.height*4/8,Screen.width/2,Screen.height/8),"Back to Main Menu"))
				{
					Application.LoadLevel ("Story" + PlayerPrefs.GetInt("level"));
				}
			}

        }
		GUI.skin = Myskin;

		Font Comic = (Font)Resources.Load ("COMICS");

		GUIStyle itemsStyle = new GUIStyle ();
		itemsStyle.normal.textColor = Color.red;
		itemsStyle.fontSize = 60;
		itemsStyle.padding.left = 115;
		itemsStyle.padding.top = 10;
		itemsStyle.font = Comic;

		GUIStyle okStyle = new GUIStyle ();
		okStyle.normal.textColor = Color.yellow;
		okStyle.fontSize = 60;
		okStyle.padding.top = 10;
		okStyle.font = Comic;


        foreach (KeyValuePair<string, bool> p in placedWords)
        {
            GUILayout.BeginHorizontal();

			GUILayout.Label(p.Key, itemsStyle);
            if (p.Value)
            {
				GUILayout.Label(" - ok!", okStyle);
            }
            GUILayout.EndHorizontal();
        }
		GUI.skin = null;
        GUILayout.EndVertical();
    }
}
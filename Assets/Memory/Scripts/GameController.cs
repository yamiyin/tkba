﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameController : MonoBehaviour 
{
	public Sprite[] spriteCardsBack;
	public Sprite[] spriteCardsFront;
	public Sprite spriteCardShadow;
	public int oldHighscore;

	[Range(1f, 16f)]
	public float uncoverTime = 4f;


	[Range(1f, 16f)]
	public float dealTime = 4f;

	[Range(0.1f, 10f)]
	public float checkPairTime = 0.5f;


	[Range(2, 32)]
	public int cardsPadding = 4;

	int pairCount = 2;


	public bool shadow = true;
	[Range(-32, 32)]
	public float shadowOffsetX = 4;
	[Range(-32, 32)]
	public float shadowOffsetY = -4;

	float shadOffsetX;
	float shadOffsetY;

	int chosenCardsBack = 0;
	int[] chosenCardsFront;

	Vector3 dealingStartPosition = new Vector3(-12800, -12800, -8);


	int totalMoves = 0;
	int bestMoves = 0;
	int uncoveredCards = 0;
	Transform[] selectedCards = new Transform[2];



	int oldPairCount;


	bool isTouching = false;
	bool isUncovering = false;
	bool isDealing = false;


	public GUISkin skin;


	public AudioClip soundDealCard;
	public AudioClip soundButtonClick;
	public AudioClip soundUncoverCard;
	public AudioClip soundFoundPair;
	public AudioClip soundNoPair;

	int stagelevel;

	void Start () 
	{
		stagelevel = PlayerPrefs.GetInt ("level");
		string user = PlayerPrefs.GetString ("CurrentUser");
		int level = Application.loadedLevel - 43;
		oldHighscore = PlayerPrefs.GetInt(user + "level");
		if (stagelevel == 1 && !(isDealing || isUncovering)) {
			pairCount = 4;
			CreateDeck ();
		}
		
		if (stagelevel == 2 && !(isDealing || isUncovering)) {
			pairCount = 4;
			CreateDeck ();
		}
		
		if (stagelevel == 3 && !(isDealing || isUncovering)) {
			pairCount = 4;
			CreateDeck ();
		}
		
		if (stagelevel == 4 && !(isDealing || isUncovering)) {
			pairCount = 4;
			CreateDeck ();
		}
		
		if (stagelevel == 5 && !(isDealing || isUncovering)) {
			pairCount = 6;
			CreateDeck ();
		}
		
		if (stagelevel == 6 && !(isDealing || isUncovering)) {
			pairCount = 6;
			CreateDeck ();
		}
		
		if (stagelevel == 7 && !(isDealing || isUncovering)) {
			pairCount = 6;
			CreateDeck ();
		}
		
		if (stagelevel == 8 && !(isDealing || isUncovering)) {
			pairCount = 6;
			CreateDeck ();
		}
		
		if (stagelevel == 9 && !(isDealing || isUncovering)) {
			pairCount = 8;
			CreateDeck ();
		}
		
		if (stagelevel == 10 && !(isDealing || isUncovering)) {
			pairCount = 8;
			CreateDeck ();
		}
		
		if (stagelevel == 11 && !(isDealing || isUncovering)) {
			pairCount = 8;
			CreateDeck ();
		}
		
		if (stagelevel == 12 && !(isDealing || isUncovering)) {
			pairCount = 8;
			CreateDeck ();
		}



	}

	void OnGUI()
	{
				if (skin != null)
						GUI.skin = skin;


				if (Application.loadedLevel == 1) {
						GUI.BeginGroup (new Rect (5, 5, 160, Screen.height - 10));
						{
								GUI.Box (new Rect (0, 0, 160, Screen.height - 10), "");

								
								if (GUI.Button (new Rect (5, 5, 150, 48), "1") && !(isDealing || isUncovering)) {
										if (soundButtonClick != null)
												GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
										pairCount = 2;
										CreateDeck ();
								}

								if (oldHighscore >= 2) {
										if (GUI.Button (new Rect (5, 55, 150, 48), "2") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 3;
												CreateDeck ();
										}
								}
								if (oldHighscore >= 3) {
										if (GUI.Button (new Rect (5, 105, 150, 48), "3") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 4;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 4) {
										if (GUI.Button (new Rect (5, 155, 150, 48), "4") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 5;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 5) {
										if (GUI.Button (new Rect (5, 205, 150, 48), "5") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 6;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 6) {
										if (GUI.Button (new Rect (5, 255, 150, 48), "6") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 7;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 7) {
										if (GUI.Button (new Rect (5, 305, 150, 48), "7") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 8;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 8) {
										if (GUI.Button (new Rect (5, 355, 150, 48), "8") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 9;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 9) {
										if (GUI.Button (new Rect (5, 405, 150, 48), "9") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 10;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 10) {
										if (GUI.Button (new Rect (5, 455, 150, 48), "10") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 11;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 11) {
										if (GUI.Button (new Rect (5, 505, 150, 48), "11") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 12;
												CreateDeck ();
										}
								}

								if (oldHighscore >= 12) {
										if (GUI.Button (new Rect (5, 555, 150, 48), "12") && !(isDealing || isUncovering)) {
												if (soundButtonClick != null)
														GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
												pairCount = 13;
												CreateDeck ();
										}
								}
								if (GUI.Button (new Rect (5, 605, 150, 48), "Home") && !(isDealing || isUncovering)) {
										if (soundButtonClick != null)
												GetComponent<AudioSource>().PlayOneShot (soundButtonClick);
										Application.LoadLevel (56);
								}




								//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
								//	GUI.skin.label.fontSize = 16;
								//	GUI.Label(new Rect(16, 80, 128, 32), "Pairs " + pairCount);

								//	pairCount = (int)GUI.HorizontalSlider(new Rect(16, 112, 128, 32), pairCount, 2, spriteCardsFront.Length);

								//	if(pairCount != oldPairCount)
								//	{
								//		oldPairCount = pairCount;

								//		bestMoves = PlayerPrefs.GetInt("Memory_" + pairCount + "_Pairs", 0);
								//	}

								//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
								//	GUI.skin.label.fontSize = 16;
								//	GUI.Label(new Rect(16, 160, 128, 32), "Moves: " + totalMoves);

								//	GUI.skin.label.alignment = TextAnchor.UpperLeft;
								//	GUI.skin.label.fontSize = 16;
								//	GUI.Label(new Rect(16, 192, 128, 32), "Best: " + bestMoves);

						}
						GUI.EndGroup ();
				}
		}

	void CreateDeck()
	{
		isDealing = true;


		DestroyImmediate(GameObject.Find("DeckParent"));
		DestroyImmediate(GameObject.Find("Temp"));
		selectedCards = new Transform[2];
		totalMoves = 0;
		uncoveredCards = 0;



		bestMoves = PlayerPrefs.GetInt("Memory_" + pairCount + "_Pairs", 0);


		chosenCardsBack = Random.Range(0, spriteCardsBack.Length);


		List<int> tmp = new List<int>();
		for(int i = 0; i < spriteCardsFront.Length; i++)
		{
			tmp.Add(i);
		}
		tmp.Shuffle();
		chosenCardsFront = tmp.GetRange(0, pairCount).ToArray();

		GameObject deckParent = new GameObject("DeckParent"); 
		GameObject temp = new GameObject("Temp");

		int cur = 0;

		float minX = Mathf.Infinity;
		float maxX = Mathf.NegativeInfinity;
		float minY = Mathf.Infinity;
		float maxY = Mathf.NegativeInfinity;


		int cCards = pairCount * 2;
		int cols = (int)Mathf.Sqrt(cCards);
		int rows = (int)Mathf.Ceil(cCards / (float)cols);

		List<int> deck = new List<int>();
		for(int i = 0; i < pairCount; i++)
		{
			deck.AddRange(new int[] {i, i});
		}
		deck.Shuffle();

		int cardWidth = 0;
		int cardHeight = 0;

		for(int x = 0; x < rows; x++)
		{
			for(int y = 0; y < cols; y++)
			{
				if(cur > cCards-1)
					break;


				GameObject card = new GameObject("Card"); 
				GameObject cardFront = new GameObject("CardFront");
				GameObject cardBack = new GameObject("CardBack");
				GameObject destination = new GameObject("Destination");

				cardFront.transform.parent = card.transform; 
				cardBack.transform.parent = card.transform; 


				cardFront.AddComponent<SpriteRenderer>();
				cardFront.GetComponent<SpriteRenderer>().sprite = spriteCardsFront[chosenCardsFront[deck[cur]]];
				cardFront.GetComponent<SpriteRenderer>().sortingOrder = -1;


				cardBack.AddComponent<SpriteRenderer>();
				cardBack.GetComponent<SpriteRenderer>().sprite = spriteCardsBack[chosenCardsBack];
				cardBack.GetComponent<SpriteRenderer>().sortingOrder = 1;

				cardWidth = (int)spriteCardsBack[chosenCardsBack].rect.width;
				cardHeight = (int)spriteCardsBack[chosenCardsBack].rect.height;


				card.tag = "Card";
				card.transform.parent = deckParent.transform;
				card.transform.position = dealingStartPosition;
				card.AddComponent<BoxCollider2D>();
				card.GetComponent<BoxCollider2D>().size = new Vector2(cardWidth, cardHeight);
				card.AddComponent<CardProperties>().Pair = deck[cur];

				destination.transform.parent = temp.transform;
				destination.tag = "Destination";
				destination.transform.position = new Vector3(x * (cardWidth + cardsPadding), y * (cardHeight + cardsPadding));

				if(shadow)
				{
					GameObject cardShadow = new GameObject("CardShadow");

					cardShadow.tag = "CardShadow";
					cardShadow.transform.parent = deckParent.transform;
					cardShadow.transform.position = dealingStartPosition;
					cardShadow.AddComponent<SpriteRenderer>();
					cardShadow.GetComponent<SpriteRenderer>().sprite = spriteCardShadow;
					cardShadow.GetComponent<SpriteRenderer>().sortingOrder = -2;
				}
				cur++;


				Vector3 pos = destination.transform.position;
				minX = Mathf.Min(minX, pos.x - cardWidth);
				minY = Mathf.Min(minY, pos.y - cardHeight);
				maxX = Mathf.Max(maxX, pos.x + cardWidth + shadowOffsetX);
				maxY = Mathf.Max(maxY, pos.y + cardHeight + shadowOffsetY);
			}
		}


		float tableScale = (GameObject.Find("Table") == null) ? 1f : GameObject.Find("Table").transform.localScale.x;
		float scale = tableScale / (maxX + cardsPadding);

		Vector2 point = LineIntersectionPoint(
			new Vector2(minX, maxY),
			new Vector2(maxX, minY),
			new Vector2(minX, minY),
			new Vector2(maxX, maxY)
			);

		temp.transform.position -= new Vector3(point.x * scale, point.y * scale);

		shadOffsetX = shadowOffsetX * scale;
		shadOffsetY = shadowOffsetY * scale;

		deckParent.transform.localScale = new Vector3(scale, scale, scale);
		temp.transform.localScale = new Vector3(scale, scale, scale);

		DealCards ();
	}

	void DealCards()
	{
		StartCoroutine (dealCards ());
	}

	IEnumerator dealCards()
	{
		GameObject[] cards = GameObject.FindGameObjectsWithTag("Card");
		GameObject[] cardsShadow = GameObject.FindGameObjectsWithTag("CardShadow");
		GameObject[] destinations = GameObject.FindGameObjectsWithTag("Destination");

		for(int i = 0; i < cards.Length; i++)
		{
			float t = 0; 

			if(soundDealCard != null)
				GetComponent<AudioSource>().PlayOneShot(soundDealCard);

			while(t < 1f)
			{
				t += Time.deltaTime * dealTime;

				cards[i].transform.position = Vector3.Lerp(
					dealingStartPosition, destinations[i].transform.position, t);

				if(cardsShadow.Length > 0)
				{
					cardsShadow[i].transform.position = Vector3.Lerp(
						dealingStartPosition, 
						destinations[i].transform.position + new Vector3(shadOffsetX, shadOffsetY), t);
				}

				yield return null;
			}
			yield return null;
		}

		isDealing = false;

		yield return 0;
	}

	void UncoverCard(Transform card)
	{
		StartCoroutine (uncoverCard(card, true));
	}

	IEnumerator uncoverCard(Transform card, bool uncover)
	{
		isUncovering = true;

		float minAngle = uncover ? 0 : 180;
		float maxAngle = uncover ? 180 : 0; 

		float t = 0;
		bool uncovered = false;

		if(soundUncoverCard != null)
			GetComponent<AudioSource>().PlayOneShot(soundUncoverCard);


		var shadow = GameObject.FindGameObjectsWithTag("CardShadow").Where(
			g => (g.transform.position == card.position + new Vector3(shadOffsetX, shadOffsetY))).FirstOrDefault();

		while(t < 1f)
		{
			t += Time.deltaTime * uncoverTime;

			float angle = Mathf.LerpAngle(minAngle, maxAngle, t);
			card.eulerAngles = new Vector3(0, angle, 0);

			if(shadow != null)
				shadow.transform.eulerAngles = new Vector3(0, angle, 0);

			if( ( (angle >= 90 && angle < 180) || 
			      (angle >= 270 && angle < 360) ) && !uncovered)
			{
				uncovered = true;
				for(int i = 0; i < card.childCount; i++)
				{

					Transform c = card.GetChild(i);
					c.GetComponent<SpriteRenderer>().sortingOrder *= -1;

					yield return null;
				}
			}

			yield return null;
		}


		if(uncoveredCards == 2)
		{

			if(selectedCards[0].GetComponent<CardProperties>().Pair !=
			   selectedCards[1].GetComponent<CardProperties>().Pair)
			{
				if(soundNoPair != null)
					GetComponent<AudioSource>().PlayOneShot(soundNoPair);


				yield return new WaitForSeconds(checkPairTime);
				StartCoroutine (uncoverCard(selectedCards[0], false));
				StartCoroutine (uncoverCard(selectedCards[1], false));
			}
			else
			{
				if(soundFoundPair != null)
					GetComponent<AudioSource>().PlayOneShot(soundFoundPair);


				selectedCards[0].GetComponent<CardProperties>().Solved = true;
				selectedCards[1].GetComponent<CardProperties>().Solved = true;
			}
			selectedCards[0].GetComponent<CardProperties>().Selected = false;
			selectedCards[1].GetComponent<CardProperties>().Selected = false;
			uncoveredCards = 0;
			totalMoves++;

			yield return new WaitForSeconds(0.1f);
		}


		if(IsSolved())
		{
			string user = PlayerPrefs.GetString ("CurrentUser");
			int score = PlayerPrefs.GetInt(user + "memoryscore" + stagelevel, 0);
			if(score > totalMoves || score == 0)
			{
				bestMoves = totalMoves;
			}
			PlayerPrefs.SetInt(user + "memoryscore" + stagelevel, bestMoves);

			Application.LoadLevel ("Story" + stagelevel);
		}

		isUncovering = false;
		yield return 0;
	}

	bool IsSolved()
	{
		foreach(GameObject g in GameObject.FindGameObjectsWithTag("Card"))
		{
			if(!g.GetComponent<CardProperties>().Solved)
				return false;
		}

		return true;
	}
	

	void Update () 
	{
		if(isDealing)
			return;

		if((Input.GetMouseButtonDown(0) || Input.touchCount > 0) && !isTouching && !isUncovering && uncoveredCards < 2)
		{
			isTouching = true;

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);


			if (hit.collider != null)
			{
				if(!hit.collider.GetComponent<CardProperties>().Selected)
				{

					if(!hit.collider.GetComponent<CardProperties>().Solved)
					{

						UncoverCard(hit.collider.transform);
						selectedCards[uncoveredCards] = hit.collider.transform;
						selectedCards[uncoveredCards].GetComponent<CardProperties>().Selected = true;
						uncoveredCards += 1;
					}
				}
			}
		}
		else
		{
			isTouching = false;
		}
	}

	Vector2 LineIntersectionPoint(Vector2 ps1, Vector2 pe1, Vector2 ps2, Vector2 pe2)
	{

		float A1 = pe1.y-ps1.y;
		float B1 = ps1.x-pe1.x;
		float C1 = A1*ps1.x+B1*ps1.y;
		

		float A2 = pe2.y-ps2.y;
		float B2 = ps2.x-pe2.x;
		float C2 = A2*ps2.x+B2*ps2.y;
		

		float delta = A1*B2 - A2*B1;
		if(delta == 0)
			return new Vector2();
		

		return new Vector2(
			(B2*C1 - B1*C2)/delta,
			(A1*C2 - A2*C1)/delta
			);
	}

	public void BacktoGameSelect(){
		Application.LoadLevel ("Story" + stagelevel);
	}

	public void BacktoQuest(){
		Application.LoadLevel ("BibleQuests");
	}
}
